'use strict';

/**
 * cookie configs
 */
export default {
    domain: "", // cookie domain
    path: "/", // cookie path
    httponly: false, //是否 http only
    secure: false, //是否在 https 下使用
    timeout: 7 * 24 * 3600 //cookie 有效时间
};