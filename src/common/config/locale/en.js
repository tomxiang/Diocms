'use strict';

export default {
    validate_required: "{name} can not be blank",
    validate_contains: "{name} need contains {args}",
    1001: "get data error",
    GET_DATA_ERROR: [1234, "get data error"] //key 必须为大写字符或者下划线才有效
};