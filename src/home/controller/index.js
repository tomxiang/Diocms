'use strict';

import Base from './base.js';
var moment = require('moment');
export default class extends Base {
    /**
     * index action
     * @return {Promise} []
     */
   async  indexAction() {
        //auto render template file index_index.html
        let cookie = this.cookie("thinkjs");
        //return this.display();
        let userinfo = await this.session("userInfo");
        this.success(userinfo);
        //return this.display();
    }

    /**
     * detail action
     * @return {Promise} []
     */
    detailAction() {
        let data = {name: "thinkjs", time: "ssss"};
        let name = this.get("name");//获取get参数
        let allParams = this.get(); //获取所有 GET 参数
        let ip = this.ip();
        this.fail(1001,"connect error");
        //this.success(name);
        //this.cookie("think_name", ip, {
        //  timeout: 3600 * 24 * 7 //有效期为一周
        //});
        //this.write(data);
        //auto render template file index_detail.html

        this.assign({
            name: "timemmm",
            desc: "timemmmxxxxxxxx"
        })
        let creat_time = moment().format("YYYY-MM-DD HH:mm:ss");
        let user = this.model("auser");
        //this.success(creat_time);
        //return this.display();
    }

    /**
     * add action
     */

    async  addAction() {
        let model = this.model('auser');
        let data= await  model.add({
            "id": 6,
            "creat_time": think.datetime,
            "canshu": [
                {
                    "a": 1,
                    "b": "111"
                },
                {
                    "a": 2,
                    "b": "222"
                },
                {
                    "a": 3,
                    "b": "333"
                }
            ]
        });

        return this.success(data);
    }

    /**
     * showlist  action
     */

    async showAction() {
        let model = this.model('auser');
        let data = await model.where1();


        this.success(data);

        //return self.display();

    }

    /**
     * list action 测试获取数量
     */
    async listAction() {
        let model = this.model("auser");
        let data = await model.getMin();
        //data returns [{name: "thinkjs", email: "admin@thinkjs.org"}, ...]

        this.success(data);
    }

    /**
     *upload action
     */
    async uploadAction(self){
        let model = this.model("auser");
        //更新数据.然后取数据
        await model.where({_id: "56d06a0bebaefe440526d251"}).update({id: "9"});
        let data =await model.where({_id: "56d06a0bebaefe440526d251"}).find();

        self.success(data);
    }

    /**
     * use base js 引用
     */
   async baseAction(){
        //let data= this.base();
        let content = await this.fetch("detail");
        let dioname=think.config("dioname",undefined,"home");

        return this.success(dioname);

    }


}