'use strict';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
    /**
     * index action logic
     * @return {} []
     */
    indexAction() {
        var rnd = Math.random();
        //console.log("test:"+rnd);

    }
    /**
     * detail action logic
     * @return {} []
     */
    detailAction() {
        this.allowMethods = "get"; //只允许 POST 请求类型
        this.rules = {
            doc: "string|default:diocms",
            version: "string|in:1.2,2.0|default:2.0"
        }

    }


}